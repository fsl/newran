include ${FSLCONFDIR}/default.mk

PROJNAME   = newran
SOFILES    = libfsl-newran.so
TESTXFILES = tryrand
LIBS       = -lm

all: ${SOFILES}
tests: all ${TESTXFILES}

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

libfsl-newran.so: hist.o newran.o extreal.o myexcept.o
	$(CXX) ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

OBJ_T = tryrand.o tryrand1.o tryrand2.o tryrand3.o tryrand4.o

tryrand: $(OBJ_T) libfsl-newran.so
	$(CXX)  ${CXXFLAGS} -o $@ ${OBJ_T} -lfsl-newran ${LDFLAGS}
